# -*- coding: utf-8 -*-
import logging

from openerp.osv import orm,osv, fields
from openerp import SUPERUSER_ID
from openerp.addons import decimal_precision
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import easypost




class SaleOrder(orm.Model):
    _inherit = 'sale.order'
    
    
    def _get_website_data(self, cr, uid, order, context=None):
        """ Override to add delivery-related website data. """
        print "order>>>>>>>>>>>>" , order
        print "order>>>>>>>>>>>>" , order.order_line
        weight = 0.0
        for order_line in order.order_line:
            if order_line.product_id.type != 'service':
                weight += order_line.product_id.weight
        
        
        delivery_carrier_obj = self.pool.get('delivery.carrier')
        res_partner_obj = self.pool.get('res.partner')
        product_tmpl_obj = self.pool.get('product.template')
        website_obj = self.pool.get('website.published.mixin')
        easypost.api_key = "5WBoXcRU4Ry7IzDgIJ06hA"
        
#----------------------------------Api calling-------------------------------------------- 
        fromAddress = easypost.Address.create(
            company = order.company_id.name or '',
            street1 = order.company_id.partner_id.street or '',
            street2 = order.company_id.street2 or '',
            city = order.company_id.city or '',
            state = order.company_id.state_id.name or '',
            zip = order.company_id.zip or '',
            phone = order.company_id.phone or ''
          )

        toAddress = easypost.Address.create(
          name = order.partner_id.name or '',
          company = order.company_id.name or '',
          street1 = order.partner_id.street or '',
          city = order.partner_id.city or '',
          state = order.partner_id.state_id.name or '',
          zip = order.partner_id.zip or ''
        )

#        fromAddress = easypost.Address.create(
#                        company = 'EasyPost',
#                        street1 = '417 Montgomery Street',
#                        street2 = '5th Floor',
#                        city = 'San Francisco',
#                        state = 'CA',
#                        zip = '94104',
#                        phone = '415-528-7555'
#                      )
#
#        toAddress = easypost.Address.create(
#                        name = 'George Costanza',
#                        company = 'Vandelay Industries',
#                        street1 = '1 E 161st St.',
#                        city = 'Bronx',
#                        state = 'NY',
#                        zip = '10451'
#                      )

        parcel = easypost.Parcel.create(
          length = 9,
          width = 6,
          height = 2,
          weight = weight
        )

        shipment = easypost.Shipment.create(
          to_address = toAddress,
          from_address = fromAddress,
          parcel = parcel
        )
        
#        print "--------fromAddress---------",fromAddress
#        print "--------toAddress---------",toAddress
#        print "--------shipment---------",(shipment)
#        print "--------shipment---------",(shipment['rates'])

        for rate in shipment.rates:
            print "-----------------carrier-------------------", rate.carrier
            print "-----------------service-------------------",rate.service
            print "-----------------rate-------------------",rate.rate
            print "-----------------id-------------------",rate.id
#----------------------------------Api calling over-------------------------------------------- 
        name_list = {}
        for rates in (shipment['rates']):
            print "=======================" , rates.get('carrier')
            print "=======================" , rates.get('service')
            rate = rates.get('rate')
            name = rates.get('carrier') +" "+rates.get('service')
            print "name>>>>>>>" , name
#            name_list.append(name)
            delivery_carrier_ids = delivery_carrier_obj.search(cr,SUPERUSER_ID,[('name','=',name)])
            
            res_partner_id = res_partner_obj.search(cr,SUPERUSER_ID,[('name','=','Transporter (Delivery provider)')])
            res_partner_browse_id=res_partner_obj.browse(cr,SUPERUSER_ID,res_partner_id)
            name_list.update({name:rate})
            print "name_list>>>---------------->>>>" , name_list
            if not delivery_carrier_ids:
                vals={'name':name,
                        'partner_id':res_partner_browse_id.id,
                        'website_published':True}
                delivery_carrier_id = delivery_carrier_obj.create(cr,SUPERUSER_ID,vals)
                
                prod_vals = {'type':'service',}
                product_tmpl_id = product_tmpl_obj.search(cr,SUPERUSER_ID,[('name','=',name)])
                product_tmpl_browse_ids=product_tmpl_obj.browse(cr,SUPERUSER_ID,product_tmpl_id)
                write_id = product_tmpl_browse_ids.write(prod_vals)
        
#        for order_line in order.order_line:
#            if order_line.product_id.type != 'service':
#                weight = order_line.product_id.weight
                
                
                
        values = super(SaleOrder, self)._get_website_data(cr, uid, order, context=context)
        # We need a delivery only if we have stockable products
        has_stockable_products = False
        for line in order.order_line:
            if line.product_id.type in ('consu', 'product'):
                has_stockable_products = True
        if not has_stockable_products:
            return values

        delivery_ctx = dict(context, order_id=order.id)
        DeliveryCarrier = self.pool.get('delivery.carrier')
        delivery_ids = self._get_delivery_methods(cr, uid, order, context=context)
        values['deliveries'] = DeliveryCarrier.browse(cr, SUPERUSER_ID, delivery_ids, context=delivery_ctx)
        print "values>>deliveries>>>>" , values['deliveries']
        print "values>>>>>>" , values
        print "name_list>>>>>>" , name_list
        
        for key, value in name_list.iteritems():
            print "list>>>>>>>>>>" , key
            print "list>>>>>>>>>>" , value
#            if re.name == rates.get('carrier') +" "+rates.get('service'):
#                re.write({'fixed_price':''})
            rent_id = DeliveryCarrier.search(cr, SUPERUSER_ID,[('name','=',key)])
            print "rent_id>>>>>" , rent_id
            if rent_id:
                DeliveryCarrier_browse_id=DeliveryCarrier.browse(cr,SUPERUSER_ID,rent_id)
                DeliveryCarrier_browse_id.write({'fixed_price':value})
            
                
        
        return values
    
    
