# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Carriers Listing',
    'version': '1.0',
    'sequence': 14,
    'summary': 'Carriers Listing',
    'description': """
Manage your sales reports
=========================
With this module you can personnalize the sale order and invoice report with
separators, page-breaks or subtotals.
    """,
    'website': 'https://www.odoo.com/page/crm',
    'depends': ['base','calendar','sale','product','account','website_sale_delivery','website'],
    'data': [
#    'views/website_sale_delivery.xml',
             ],
#    'demo': ['data/sale_layout_category_data.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
